﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NorthwindEntities ne = new NorthwindEntities();

        public MainWindow()
        {
            InitializeComponent();
            this.Tabelike.ItemsSource = ne.Products.ToList();
        }

        private void Otsi_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                decimal.TryParse(this.MaxHind.Text, out decimal maxHind);
                decimal.TryParse(this.MinHind.Text, out decimal minHind);
                this.Tabelike.ItemsSource =
                    ne.Products
                    .Where(x => 
                        x.UnitPrice <= (maxHind == 0 ? decimal.MaxValue : maxHind)
                        &&
                        x.UnitPrice >= (minHind)

                        )
                    .ToList();
            }
            catch
            {
                this.Tabelike.ItemsSource =
                    ne.Products
                    .ToList();
            }
        }
    }
}
